import numpy
from pyscf import gto, dft
from pyscf.dft import numint
import time
import numpy as np
from LHUKS import UKS
from LHRKS import RKS
from local_hybrid import Uget_xcmat_LH,get_xcmat_LH

#Dictionary with the atoms and it's total spin
atoms={"H":1,"He":0,"Li":1,
        "Be":0,"B":1,
         "C":2,"N":3,
        "O":2,"F":1,"Ne":0,"Na":1,"Ne":0,"Na":1,
        "Mg":0,"Al":1,"Si":2,"P":3,"S":2,"Cl":1,
        "Ar":0}
#atoms={"He":0, "Be":0}

def calc_E_atom(atom):

       mol = gto.M(
       verbose = 0,
       atom =atom,
       spin=atoms[atom],
       basis = 'cc-pvtz')

       if mol.spin != 0:
          mf = UKS(mol)
          mf.Uget_xcmat_LH = Uget_xcmat_LH
       else:
          mf = RKS(mol)
          mf.get_xcmat_LH = get_xcmat_LH
       mf.xc = 'HF,'
       mf.kernel()
       E = mf.e_tot
       edict = {atom:E}
       print(edict)
       return edict


results = [calc_E_atom(atom) for atom in atoms]

#to convert the list of dictionaries to a dictionary
results_dict = {}
for sub_dict in results:
    results_dict.update(sub_dict)

#creation of the file
f=open("E_atom.txt","w")
f.write("Atom "+'LH'+"\n")
for atom in results_dict:
    f.write(atom +" %.8f\n"%results_dict[atom])
f.close()
