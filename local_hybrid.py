#!/usr/bin/env python
#!/usr/bin/env python

import numpy
from pyscf import gto, dft
from pyscf.dft import numint
import time
import numpy as np
import pyscf
from pyscf import lib
from pyscf.lib import logger
from pyscf import scf
from pyscf.scf import hf
from pyscf.scf import _vhf
from pyscf.scf import jk
from pyscf.dft import gen_grid
from pyscf.dft import numint
from pyscf import __config__
from LHUKS import UKS
from LHRKS import RKS
import hfx_density as hfx

'''
Evaluate exchange-correlation functional and its potential on given grid
coordinates.
'''

def get_hcore(mol):
    '''Core Hamiltonian

    Examples:

    >>> from pyscf import gto, scf
    >>> mol = gto.M(atom='H 0 0 0; H 0 0 1.1')
    >>> scf.hf.get_hcore(mol)
    array([[-0.93767904, -0.59316327],
           [-0.59316327, -0.93767904]])
    '''
    h = mol.intor_symmetric('int1e_kin')

    if mol._pseudo:
        # Although mol._pseudo for GTH PP is only available in Cell, GTH PP
        # may exist if mol is converted from cell object.
        from pyscf.gto import pp_int
        h += pp_int.get_gth_pp(mol)
    else:
        h+= mol.intor_symmetric('int1e_nuc')

    if len(mol._ecpbas) > 0:
        h += mol.intor_symmetric('ECPscalar')
    return h*1.00001

def get_xcmat_LH(mol, dm,mf, *args, **kwargs):


    grids = mf.grids
    coords = grids.coords
    weights = grids.weights


    ao_values  = numint.eval_ao(mol, coords, deriv=2)
    aog = ao_values[0]

    rho = \
                        dft.numint.eval_rho(mol, ao_values, dm, xctype="MGGA")

    ex_sl, vx_sl = dft.libxc.eval_xc('SLATER,', rho)[:2]
    ec_sl, vc_sl = dft.libxc.eval_xc(',VWN', rho)[:2]

    ex_sl = ex_sl*rho[0][:]


    # Evaluate exact exchange energy density

    hfx_result = hfx.get_hf_density(mol, dm, deriv=1, coords=coords, weights=weights)
    exxa = hfx_result.exx[:][0]

    # Evaluate the mGGA part of the LH Fock matrix

    v_mgga, lmf = get_vmgga_LH(rho,2.*exxa, ex_sl, vx_sl)
    vk_mgga = numint.eval_mat(mol,ao_values,weights,rho,v_mgga, xctype='MGGA')
    vc_mgga = numint.eval_mat(mol,aog,weights,rho,vc_sl[0], xctype='LDA')

    

    ex_lh= 2.*exxa*lmf + (1.-lmf)*ex_sl
    exc_lh = ex_lh + ec_sl*rho[0][:]
    Exc = np.sum(exc_lh*weights)

    # Evaluate the exx part of the LH Fock matrix

    fxx = (hfx_result.fxx[0] + hfx_result.fxx[1] )/2
    lmfw = weights*lmf
    vx = -np.einsum('r, ri,rj -> ij', lmfw, aog, fxx)
    vx += vx.transpose()
    vx = vx/2.
    vj = hfx_result.coulomb

    return vk_mgga+vx+vc_mgga, Exc


def Uget_xcmat_LH(mol, dm,mf, *args, **kwargs):


    grids = mf.grids
    coords = grids.coords
    weights = grids.weights

    ao_values  = numint.eval_ao(mol, coords, deriv=2)
    aog = ao_values[0]

    rho_up =  dft.numint.eval_rho(mol, ao_values, dm[0], xctype="MGGA")
    rho_down =  dft.numint.eval_rho(mol, ao_values, dm[1], xctype="MGGA")

    ex_sl_up,vx_sl_up,= dft.libxc.eval_xc("Slater,", rho_up[0]*2.)[:2] 
    ex_sl_down,vx_sl_down,= dft.libxc.eval_xc("Slater,", rho_down[0]*2.)[:2]

    ec_sl, vc_sl = dft.libxc.eval_xc(',VWN', [rho_up,rho_down], spin=5)[:2]

    ex_sl_up = ex_sl_up*rho_up[0][:]
    ex_sl_down = ex_sl_down*rho_down[0][:]


    # Evaluate exact exchange energy density

    hfx_result = hfx.get_hf_density(mol, dm, deriv=1, coords=coords, weights=weights)
    exxa = hfx_result.exx[:][0]
    exxb = hfx_result.exx[:][1]
    exx = exxa+exxb

    # Evaluate the mGGA part of the LH Fock matrix


    v_mgga_up, lmf_up = get_vmgga_LH(rho_up,exxa, ex_sl_up, vx_sl_up)
    vk_mgga_up = numint.eval_mat(mol,ao_values,weights,rho_up,v_mgga_up, xctype='MGGA')

    v_mgga_down, lmf_down = get_vmgga_LH(rho_down,exxb, ex_sl_down, vx_sl_down)
    vk_mgga_down = numint.eval_mat(mol,ao_values,weights,rho_down,v_mgga_down, xctype='MGGA')

    vc_mgga_up = numint.eval_mat(mol,aog,weights,(rho_up[0], rho_down[0]),vc_sl[0][:,0], xctype='LDA')
    vc_mgga_down = numint.eval_mat(mol,aog,weights,(rho_down[0], rho_up[0]),vc_sl[0][:,1], xctype='LDA')


    ex_lh_up= exxa*lmf_up + (1.-lmf_up)*ex_sl_up
    if mol.nelectron != 1:
       ex_lh_down= exxb*lmf_down + (1.-lmf_down)*ex_sl_down
    else:
       ex_lh_down= ex_lh_up*0.
    ex_lh = ex_lh_up +ex_lh_down
    exc_lh = ex_lh + ec_sl*(rho_up[0]+rho_down[0])
    Exc = np.sum(exc_lh*weights)
    #print('Exc',Exc)

    # Evaluate the exx part of the LH Fock matrix

    fxxa = hfx_result.fxx[0]
    lmfw_u = weights*lmf_up
    vxa = -np.einsum('r, ri,rj -> ij', lmfw_u, aog, fxxa)
    vxa += vxa.transpose()
    vxa = vxa/2.

    fxxb = hfx_result.fxx[1]
    lmfw_d = weights*lmf_down
    vxb = -np.einsum('r, ri,rj -> ij', lmfw_d, aog, fxxb)
    vxb += vxb.transpose()
    vxb = vxb/2.

    exa,exb = hfx_result.exchange
    exa = -exa 
    exb = -exb

    vk_mgga_up += vk_mgga_up.transpose()
    vk_mgga_up = vk_mgga_up*0.5
    vk_mgga_down += vk_mgga_down.transpose()
    vk_mgga_down = vk_mgga_down*0.5

    if mol.nelectron == 1:
       vxb = vxa*0.
       vk_mgga_down = vxa*0.
       vc_mgga_down = vxa*0.

    return np.array([vxa+vk_mgga_up+vc_mgga_up,vxb+vk_mgga_down+vc_mgga_down]), Exc


def get_lmf(rho_array):

        ap = 0.48

        rho,dx_rho,dy_rho,dz_rho,lap,tau = rho_array
        G = dx_rho**2+dy_rho**2+dz_rho**2
        tauw = G/(8.*rho)
        tauratio = tauw/tau

        vrho =  -tauratio/rho*ap
        vsigma = ap* 1./(8.*tau*rho) 
        vtau =  -tauratio/tau*ap

        vlmf= (vrho, vsigma, vrho*0., vtau)
        return ap*tauratio, vlmf


def get_vmgga_LH(rho_array,exx, ex_sl, vx_sl):

    lmf, vlmf = get_lmf(rho_array)
    vlmf_rho, vlmf_sigma, vlmf_lap, vlmf_tau = vlmf

    vrho, vsigma, vlap, vtau = vlmf_rho*exx, vlmf_sigma*exx, vlmf_lap*exx, vlmf_tau*exx
    vrho += -vlmf_rho*ex_sl
    vsigma += -vlmf_sigma*ex_sl
    vlap += -vlmf_lap*ex_sl
    vtau += -vlmf_tau*ex_sl

    vrho+= (1.-lmf)*vx_sl[0] 

    v_mgga = (vrho, vsigma, vlap*0., vtau)
    return v_mgga, lmf

    


# Hellmann-Feynmann Test 

if __name__ == '__main__':

     lib.num_threads(1)
     mol = gto.M(
       verbose = 5,
       atom ='Ne',
       #spin=1,
       basis = 'cc-pvtz')

     mf = UKS(mol)
     #mf.get_hcore = get_hcore
     mf.Uget_xcmat_LH = Uget_xcmat_LH
     mf.xc = 'HF,'
     mf.kernel()
     E0 = mf.e_tot

     dm = mf.make_rdm1()
     dm = dm[0]+dm[1]
     hc = mf.get_hcore(mf.mol)
     Ecore = np.einsum('ij,ij',dm,hc)

     mol = gto.M(
        verbose = 5,
        atom ='Ne',
        #spin=1,
        basis = 'cc-pvtz')

     mf = UKS(mol)
     mf.get_hcore = get_hcore
     mf.Uget_xcmat_LH = Uget_xcmat_LH
     mf.xc = 'HF,'
     mf.kernel()
     E1 = mf.e_tot 


     print((E1-E0)/0.00001, Ecore)




